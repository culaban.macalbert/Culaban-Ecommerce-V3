import { Button } from 'react-bootstrap';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import { Link } from 'react-router-dom';
import './Announcement.css'

export default function Announcement() {
  return (
    <Row xs={12} md={3} className="g-4 mainContainerAnnouncement">
      <Card className="bg-dark text-white">
        <Card.Img src="https://cdn.mos.cms.futurecdn.net/pfEc8UcpazumarfpgbNtmi.gif" alt="Card image" />
          <Card.ImgOverlay>
            <Button variant="outline-info" as={Link} to="/products">BUY ME NOW</Button>
          </Card.ImgOverlay>
      </Card>

      <Card className="bg-dark text-white">
        <Card.Img src="https://blog.finishline.com/wp-content/uploads/2018/04/LeBron-XV-Low.gif" alt="Card image" />
            <Card.ImgOverlay>
             <Button variant="outline-info" as={Link} to="/products">BUY ME NOW</Button>
           </Card.ImgOverlay>
      </Card>

      <Card className="bg-dark text-white">
        <Card.Img className='image3' src="https://thumbs.gfycat.com/LightheartedUnnaturalHarrier-size_restricted.gif" alt="Card image" />
          <Card.ImgOverlay>
             <Button variant="outline-info" as={Link} to="/products">BUY ME NOW</Button>
          </Card.ImgOverlay>
      </Card>
    </Row>
  );
}

