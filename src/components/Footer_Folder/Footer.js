import { Fragment } from "react";
import { Button, Card, Row, Col} from "react-bootstrap";
import Logo from './1.png';
import './Footer.css'

const Footer = () => {
  return (
    <Fragment>
      <Row xs={1} md={3} className='footerClass'>
        <Card className="cardFooter1">
          <Card.Body>
            <img className='LogoClass' src={Logo}/>
            <Card.Title>SENDA SHOES COLLECTION</Card.Title>
            <Card.Text>
              SShop the latest styles in footwear at SendaShoes! Our collection features trendy and comfortable shoes that you won't want to take off.
            </Card.Text>
            <Card.Title>Follow our Socials!</Card.Title>
            <img className='socialPics' src="https://www.pngitem.com/pimgs/m/296-2965005_logos-de-redes-sociales-png-transparent-png.png"/>    
          </Card.Body>
        </Card>

        <Card className="cardFooter2">
          <Card.Body>
            <Row>
            <Card.Title><h2>Useful Links</h2></Card.Title>
              <Col>
                <Card.Title>Blogs & Pages</Card.Title>
                <Card.Text>
                  culaban.Blogs
                </Card.Text>
                <Card.Text>
                  Nike
                </Card.Text>
                <Card.Text>
                  Adidas
                </Card.Text>
                <Card.Text>
                  New Balance
                </Card.Text>
              </Col>
              <Col>
                <Card.Title>Developers</Card.Title>
                <Card.Text>
                  Culaban Brothers
                </Card.Text>
                <Card.Text>
                  Mark Culaban
                </Card.Text>
                <Card.Text>
                  Albert Culaban
                </Card.Text>
                <Card.Text>
                  Michael Culaban
                </Card.Text>
              </Col>

              <Col>
                <Card.Title>Company</Card.Title>
                <Card.Text>
                  About Us
                </Card.Text>
                <Card.Text>
                  Partnerships
                </Card.Text>
                <Card.Text>
                  Careers
                </Card.Text>
              </Col>
            </Row>
          </Card.Body>
        </Card>

        <Card className="cardFooter3">
          <Card.Body>
            <Card.Title>Payment Method</Card.Title>
            <a href="https://www.paypal.com/us/home" target="_blank">
            <img className='payImage' src="https://i.ibb.co/Qfvn4z6/payment.png"/></a>

            <Card.Text className="infocard3">
            East Villas Subd., Brgy. Estefania, Bacolod City, Negros Occidental
            culaban.mac@developer.to
            </Card.Text>
          <a href="https://play.google.com/store/games?hl=en&gl=US" target="_blank">
            <img className='googleImage' src="https://i.pinimg.com/736x/18/b8/03/18b8035a7933b9323f2d8f82cf400aab.jpg"/></a>
          </Card.Body>
        </Card>
      </Row>        
    </Fragment>

    
  );
}
export default Footer;