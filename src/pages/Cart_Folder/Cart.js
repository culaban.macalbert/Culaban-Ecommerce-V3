import { useEffect, useState, Fragment } from "react";
import { Button, Nav, Table } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import Swal from "sweetalert2";
import './Cart.css'


export default function Cart() {
  const [orders, setOrders] = useState([]);
  const [name, setName] = useState("");
  const [totalAmount, setTotalAmount] = useState("");
  const [hideInputs, setHideInputs] = useState(false);

  const handleCheckout = () => {
    setHideInputs(true);
  }

  useEffect(() => {

    const token = localStorage.getItem("email");
    if (!token) {
      <Nav as={NavLink} to="/login"/>
      return;
    }

    const fetchData = async () => {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/users/myOrders`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        });
        const responseData = await response.json();
        setName(responseData.name);
        setTotalAmount(responseData.totalAmount);
        setOrders(responseData.orders);
    };
    fetchData();
  }, []);

  const justSend = () => {
    Swal.fire({
      title: 'Thank you!',
      icon: 'success',
      text: 'Happy Shopping!',
    });
  };

  return (
    <div>
      <h2>{name}'s Orders</h2> 
      {!hideInputs && (
        <>

      <p>Total amount spent on all orders: {totalAmount} PHP</p>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Purchased On</th>
            <th>Product Name</th>
            <th>Quantity</th>
            <th>Subtotal</th>
          </tr>
        </thead>
        <tbody>
          {orders.map((order, index) => (
            <Fragment key={index}>
              <tr>
                <td rowSpan={order.products.length}>{order.purchasedOn}</td>
                <td>{order.products[0].name}</td>
                <td>{order.products[0].quantity}</td>
                <td rowSpan={order.products.length}>{order.subTotal} Php</td>
              </tr>
              {order.products.slice(1).map((product, index) => (
                <tr key={index}>
                  <td>{product.name}</td>
                  <td>{product.quantity}</td>
                </tr>
              ))}
            </Fragment>
          ))}
          <tr>
            <td colSpan="4">
              <b>Total amount spent on all orders: </b>{totalAmount} Php<Button className="checkOutButton" onClick={() => {justSend(); handleCheckout();}}>CheckOut</Button>
            </td>
          </tr>
        </tbody>
      </Table>
    </>
  )}
    </div>
  );
}


// //   return (
//   <div>
//   <h1>ALL ORDERS</h1>
//     <table style={{ width: '100%' }}>
//       <thead>
//         <tr>
//           <th>Name</th>
//           <th>Order Date</th>
//           <th>Products</th>
//         </tr>
//       </thead>
//       <tbody>
//         {Object.entries(orders).map(([userId, userOrders]) => (
//           <React.Fragment key={userId}>
//             {userOrders.orders.map((order) => (
//               <tr key={order._id}>
//                 <td>{userOrders.name}</td>
//                 <td>{(order.purchasedOn)}</td>
//                 <td>
//                   {order.products.map((product) => (
//                     <div key={product._id}>
//                       {product.productName} x {product.quantity}
//                     </div>
//                   ))}
//                 </td>
//               </tr>
//             ))}
//           </React.Fragment>
//         ))}
//       </tbody>
//     </table>
// </div>
// );
// }