import ProductCard from "../../components/ProductCard";
import React, { Fragment, useContext, useState, useEffect } from "react";
import UserContext from "../../UserContext";
import {Navigate, NavLink} from 'react-router-dom'
import { Row } from "react-bootstrap";
import './Products.css'

export default function Products(){

    const {user} = useContext(UserContext)

    const [products, setProducts] = useState([]);

    useEffect(() => {
        
        fetch(`${process.env.REACT_APP_API_URL}/products/active`)
        .then(res => res.json())
        .then(data => {
            console.log(data)
            setProducts(data);
        })
    }, []);

    return (
        <>
        <div className="header">
            <div className="titleProducts">
                <h1>PRODUCTS</h1>
            </div>
            
            <Row xs={1} sm={2} md={3}>
                {products.map(product => (
                    <ProductCard key={product._id} productProp={{...product}} />
                ))}
            </Row>
        </div>
        </>
    )
}
